import process from "node:process";
import mariadb, { PoolConnection } from "mariadb";
import colors from "colors";
import Config from "../lib/config";
import type { OkPacket } from "../ts/types/mariadb_types";

/** TODO instead of recreating the config object
 *  come up with a way to pass the config from main.ts to pool*/
const config: Config = new Config();

/** Create a pool to connect to the DB */
const pool = mariadb.createPool({
    host: config.db.host,
    user: config.db.user,
    password: config.db.pass,
    database: config.db.name,
    /* host: process.env.DB_HOST,
     * user: process.env.DB_USER,
     * password: process.env.DB_PASS,
     * database: process.env.DB_NAME, */
});

/** Connector Object with getConnection fn to create a connection to the DB */
const connector = {
    getConnection: () => {
        return new Promise<PoolConnection>((resolve, reject) =>{
            pool.getConnection().then((connection: PoolConnection) =>{
                resolve(connection);
            }).catch((error: unknown) => {
                reject(error);
            });
        });
    }
};

/** Clean up the DB by deleting all socketids
 * because if the server is down everyone is disconnected
 * no need to store the socketids of users */
export const dbCleanup = async () => {
    let conn: PoolConnection | undefined;

    try {
        conn = await pool.getConnection();
        if (!conn) throw console.error(colors.red(`db/db.cleanDb() => conn is undefined!`));

        const query: string = `DELETE FROM socketids`;
        const queryResult: OkPacket = await conn.query(query);

        console.log(colors.green(`DB has been cleaned.`));

    } catch (err: unknown) {
        throw console.error(colors.red(`db/db.cleanDb() => ${err}`));
    } finally {
        if (conn) conn.release();
    }
};

export default connector;
