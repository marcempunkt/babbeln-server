import { Router } from "express";

import UserController from "../controllers/UserController";
import UserProfileController from "../controllers/UserProfileController";
import UserProfileChangeController from "../controllers/UserProfileChangeController";
import UserFindbyController from "../controllers/UserFindbyController";
import UserFriendrequestController from "../controllers/UserFriendrequestController";
import UserFriendController from "../controllers/UserFriendController";
import UserFriendGetController from "../controllers/UserFriendGetController";

import authentication from "../middlewares/authentication";

const router: Router = Router();

// /user
router.post(
    "/validate-token",
    UserController.validateToken
);
router.post(
    "/register",
    UserController.registerUser
);
router.post(
    "/login",
    UserController.loginUser
);
router.delete(
    "/delete",
    authentication,
    UserController.deleteUser
);

// /user/findby
router.get(
    "/findby/name/:username",
    UserFindbyController.findbyUsername
);
router.get(
    "/findby/id/:userId",
    UserFindbyController.findbyUserid
);
router.get(
    "/findby/token/:token",
    UserFindbyController.findbyToken
);

// /user/profile
router.get(
    "/profile/get-avatar/:userId",
    UserProfileController.getAvatar
);
router.get(
    "/profile/get-status/:userId",
    UserProfileController.getStatus
);
router.get(
    "/profile/get-registerdate/:userId",
    UserProfileController.getRegisterDate
);

// /user/profile/change
router.patch(
    "/profile/change/username",
    authentication,
    UserProfileChangeController.changeUsername
);
router.patch(
    "/profile/change/email",
    authentication,
    UserProfileChangeController.changeEmail
);
router.patch(
    "/profile/change/password",
    authentication,
    UserProfileChangeController.changePassword
);
router.patch(
    "/profile/change/status",
    authentication,
    UserProfileChangeController.changeStatus
);
router.post(
    "/profile/change/avatar",
    authentication,
    UserProfileChangeController.changeAvatar
);

// /user/friend
router.delete(
    "/friend/remove",
    authentication,
    UserFriendController.removeFriend
);
router.patch(
    "/friend/block",
    authentication,
    UserFriendController.blockFriend
);
router.patch(
    "/friend/unblock",
    authentication,
    UserFriendController.unblockFriend
);

// /user/friend/get
router.get(
    "/friend/get/friends",
    authentication,
    UserFriendGetController.getAll
);
router.get(
    "/friend/get/pendings",
    authentication,
    UserFriendGetController.getPendings
);

// /user/friendrequest
router.get(
    "/friendrequest/fuzzy-search/:query",
    UserFriendrequestController.fuzzySearchUsers
);
router.post(
    "/friendrequest/send",
    authentication,
    UserFriendrequestController.sendFriendrequest
);
router.patch(
    "/friendrequest/accept/:friendreqId",
    authentication,
    UserFriendrequestController.acceptFriendrequest
);
router.delete(
    "/friendrequest/decline/:friendreqId",
    authentication,
    UserFriendrequestController.declineFriendrequest
);

export default router;
