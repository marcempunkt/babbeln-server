import { Express, Response, Request } from "express";
import colors from "colors";
import { PoolConnection } from "mariadb";
import multer from "multer";
import fs from "fs";
import path from "path";

import pool from "../db/db";
import { getAllFriendIds } from "../socket/server.utils";
import PostControllerUtils from "./PostController.Utils";
import type { OkPacket } from "../ts/types/mariadb_types";
import type { User, Post as ServerPost, PostComment as ServerPostComment } from "../ts/types/db_types";
import type { Post as ClientPost, PostComment as ClientPostComment } from "../ts/types/client_types";
import type { ResponseBody, AuthLocals } from "../ts/types/express_types";

const getAllPosts = async (
    req: Request,
    res: Response<Array<ClientPost> | ResponseBody, AuthLocals>
) => {
    /**
     * Gets all Post from you and your friends
     * 
     * @returns  {ClientPost[]}  clientPosts
     */
    let conn: PoolConnection | undefined;
    
    try {
        conn = await pool.getConnection();
        if (!conn) throw console.error(colors.red("PostController.getAllPosts() => conn is undefined!"));

        /* Check if userId does really exist in the DB */
        const queryCheckUserid: string = `SELECT * FROM users WHERE id=?`;
        const queryCheckUseridResult = await conn.query(queryCheckUserid, [res.locals.userId]);
        const user: User = queryCheckUseridResult[0];
        if (!user) {
            console.error(`/api/post/get/:token => no user found with the id ${res.locals.userId} => user: ${user}`);
            return res.status(401).send({ message: `No user found with the id ${res.locals.userId}` });
        }

        /* create sql query with the correct amounts of where id = ? */
        const allFriendIds: Array<number> = await getAllFriendIds(res.locals.userId);
        let queryGetPosts: string;

        if (allFriendIds.length) {
            queryGetPosts = `SELECT * FROM posts WHERE written_by=${res.locals.userId} OR`;
            allFriendIds.forEach((friendId: number, index: number) => {
	        if (index === allFriendIds.length -1) {
	            queryGetPosts += ` written_by=${friendId}`;
	        } else {
	            queryGetPosts += ` written_by=${friendId} OR`
	        }
            });
        } else {
            queryGetPosts = `SELECT * FROM posts WHERE written_by=${res.locals.userId}`;
        }

        /* get all posts */
        const queryGetPostsResult = await conn.query(queryGetPosts);
        const getPosts: Array<ServerPost> = [...queryGetPostsResult];

        /* add images and likes to each post */
        let clientPosts: Array<ClientPost> = await Promise.all(getPosts.map(async (post: ServerPost) => {
            if (!conn) throw console.error(colors.red("PostController.getAllPosts() => conn is undefined!"));

            /* get the likes */
            const queryGetLikes: string = `SELECT user_id FROM post_likes WHERE post_id=?`;
            const queryGetLikesResult = await conn.query(queryGetLikes, [post.id]);
            const likes: Array<number> = queryGetLikesResult.map((x: { user_id: number }) => x.user_id);

            /* get the images */
            const queryGetImages: string = `SELECT filename FROM post_images WHERE post_id=?`;
            const queryGetImagesResult = await conn.query(queryGetImages, [post.id]);
            const images: Array<string> = queryGetImagesResult.map((x: { filename: string }) => x.filename);

            /* get the comments */
            const queryGetComments: string = `SELECT id, user_id, content, written_at FROM post_comments WHERE post_id=?`;
            const queryGetCommentsResult = await conn.query(queryGetComments, [post.id]);
            const comments: Array<ClientPostComment> = [...queryGetCommentsResult].map((comment: ServerPostComment) => (
	        {
	            commentId: comment.id,
	            postId: comment.post_id,
	            userId: comment.user_id,
	            content: comment.content,
	            writtenAt: comment.written_at,
	        }
            ));

            const clientPost: ClientPost = {
	        id: post.id,
	        writtenBy: post.written_by,
	        content: post.content,
	        writtenAt: post.written_at,
	        images,
	        likes,
	        comments,
            };
            return clientPost;
        }));

        return res.send(clientPosts);

    } catch(err: unknown) {
        res.status(500).send({ message: "Couldn't get your posts." });
        throw console.log(colors.red(`PostController.getAllPosts() => ${err}`));
    } finally {
        if (conn) conn.release();
    }
};

const postPost = async (
    req: Request<{}, {}, { content: string; images: Array<string> }>,
    res: Response<ClientPost | ResponseBody,  AuthLocals>
) => {
    /**
     * Post a new post
     * 
     * @body  {string}    content
     * @body  {string[]}  images
     * 
     * @returns {ClientPost}
     */

    // @ts-ignore TODO expand the type declaration of upload & Multer to make the code below work
    PostControllerUtils.upload(req, res, async (err: Error) => {
        if (err instanceof multer.MulterError) {
            console.error(colors.red(`PostController.postImages => ${err}`));
            return res.status(500).send({ message: `${err}` });
        } else if (err) {
            console.error(colors.red(`PostController.postImages => ${err}`));
            return res.status(500).send({ message: `${err}` });
        } else if (!req.files) {
            console.error(colors.red(`PostController.postImages => no req.file found`));
            return res.status(500).send({ message: `Try uploading a .png, .jpg or .gif image the next time.` });
        }
        /* Everything went fine. */

        const writtenAt: number = new Date().valueOf();
        const writtenBy: number = res.locals.userId;
        const files: Array<Express.Multer.File> = Object.assign(req.files);

        /* post has no meaningful value for example no text no images therefore there is no need to post it */
        if (req.body.content === "" && !files.length) {
            await PostControllerUtils.deletePostPictures(files);
            return res.status(400).send({ message: `Your post has no meaningful value.` });
        }
        if (req.body.content.length > 200) {
            await PostControllerUtils.deletePostPictures(files);
            return res.status(400).send({ message: `Your post is more than 200 characters.` }); 
        }

        let conn: PoolConnection | undefined;

        try {
            conn = await pool.getConnection();
            if (!conn) {
	        await PostControllerUtils.deletePostPictures(files);
	        throw console.error(colors.red(`PostController.postPost() => conn is undefined!`));
            }

            /* Save new post to db */
            const queryPost: string = `INSERT INTO posts(written_by, content, written_at) VALUES(?, ?, ?)`;
            const queryPostResult: OkPacket = await conn.query(queryPost, [writtenBy, req.body.content, writtenAt]);
            console.log(`PostController.postPost() => posted ${JSON.stringify(queryPostResult)}`);

            /* Get the newly posted post and return it to the client */
            const queryNewPost: string = `SELECT * FROM posts WHERE written_by=? AND content=? AND written_at=?`;
            const queryNewPostResult = await conn.query(queryNewPost, [writtenBy, req.body.content, writtenAt]);
            const newPost: ServerPost | undefined = queryNewPostResult[0];
            if (!newPost) {
	        await PostControllerUtils.deletePostPictures(files);
	        return res.status(500).send({ message: `Couldn't find the new post.` });
            }

            const images: Array<string> = await PostControllerUtils.handlePostPostImages(files, newPost.id);

            const newClientPost: ClientPost = {
	        id: newPost.id,
	        writtenBy: newPost.written_by,
	        content: newPost.content,
	        writtenAt: newPost.written_at,
	        images: images,
	        likes: [],
	        comments: [],
            };

            return res.send(newClientPost);

        } catch(err: unknown) {
            res.status(500).send({ message: `Couldn't post.` });
            await PostControllerUtils.deletePostPictures(files);
            throw console.error(colors.red(`PostController.postPost() => ${err}`));
        } finally {
            if (conn) conn.release();
        }
    });
};

const deletePost = async (
    req: Request<{}, {}, { postId: number }>,
    res: Response<ServerPost | ResponseBody, AuthLocals>
) => {
    /**
     * delete a post
     * 
     * @param    {postId}  number 
     * 
     * @returns  {ServerPost}  deletedPost
     */
    let conn: PoolConnection | undefined;

    try {
        conn = await pool.getConnection();
        if (!conn) throw console.error(colors.red(`PostController.deletePost() => conn is undefined!`));

        /* Check if userId and posts author id the same is */
        const queryGetPost: string = `SELECT * FROM posts WHERE id=?`;
        const queryGetPostResult = await conn.query(queryGetPost, [req.body.postId]);
        const toDeletePost: ServerPost | undefined = [...queryGetPostResult][0];
        /* there is no post with this id */
        if (!toDeletePost) {
            console.error(colors.red(
	        `PostController.deletePost() => There is no post with the id ${req.body.postId}`
            ));
            return res.status(400).send({ message: `There is no post with the id ${req.body.postId}` });
        }

        /* you are not the author of this post */
        if (toDeletePost.written_by !== res.locals.userId) {
            console.error(colors.red(
	        `PostController.deletePost() => User with the id ${res.locals.userId} isn't the author of the post written by ${toDeletePost.written_by}.`
            ));
            return res.status(401).send({ message: `You aren't the author of this post!` });
        }

        /* delete post */
        const queryDelete: string = `DELETE FROM posts WHERE id=?`;
        const queryDeleteResult: OkPacket = await conn.query(queryDelete, [req.body.postId]);
        console.log(`PostController.deletePost() => deleted post ${JSON.stringify(queryDeleteResult)}`);
        /* delete all likes associated with this post */
        const queryDeleteLikes: string = `DELETE FROM post_likes WHERE post_id=?`;
        const queryDeleteLikesResult: OkPacket = await conn.query(queryDeleteLikes, [req.body.postId]);
        /* delete all images associated with this post */
        const queryDeleteImages: string = `DELETE FROM post_images WHERE post_id=?`;
        const queryDeleteImagesResult: OkPacket = await conn.query(queryDeleteImages, [req.body.postId]);

        /* TODO delete all images of a post in the file server */

        return res.send(toDeletePost);

    } catch(err: unknown) {
        res.status(500).send({ message: "Couldn't delete post." });
        throw console.error(colors.red(`PostController.deletePost() => ${err}`));
    } finally {
        if (conn) conn.release();
    }
};

const getPostImage = async (
    req: Request<{ filename: string }>,
    res: Response<Buffer | ResponseBody>
) => {
    /**
     * Get the image of a post but by its filename only!
     * 
     * @param    {string}  filename
     * 
     * @returns  image.gif
     */
    const filename: string = req.params.filename;
    const imagePath: string = path.join(__dirname, "../../", "files", "post", filename);

    try {
        const image: Buffer = await fs.promises.readFile(imagePath);
        return res.type("gif").send(image);
    } catch (_) {
        return res.status(500).send({ message: `Couldn't find post image.` });
    }
};

const toggleLike = async (
    req: Request<{}, {}, { postId: number }>,
    res: Response<ResponseBody, AuthLocals>
) => {
    /**
     * Like or unlike a post
     * 
     * @body     {number}  postId
     * 
     * @returns  {Success|Error}
     */
    let conn: PoolConnection | undefined;

    try {
        conn = await pool.getConnection();
        if (!conn) throw console.error(colors.red(`PostController.toggleLike() => conn is undefined!`));

        /* Check if already liked or not */
        const queryLiked: string = `SELECT * FROM post_likes WHERE user_id=? AND post_id =?`;
        const queryLikedResult = await conn.query(queryLiked, [res.locals.userId, req.body.postId]);
        const isLiked: boolean = ([...queryLikedResult][0] ? true : false);

        if (isLiked) {
            const queryRemoveLike: string = `DELETE FROM post_likes WHERE user_id=? AND post_id=?`;
            const queryRemoveLikeResult: OkPacket = await conn.query(queryRemoveLike, [res.locals.userId, req.body.postId]);
            return res.send({ message: "Disliked!" });
            
        } else {
            const queryInsertLike: string = `INSERT INTO post_likes(user_id, post_id) VALUES(?, ?)`;
            const queryInsertLikeResult: OkPacket = await conn.query(queryInsertLike, [res.locals.userId, req.body.postId]);
            return res.send({ message: "Liked!" });
        }

    } catch(err: unknown) {
        res.status(500).send({ message: "Could not like/dislike post." });
        throw console.error(colors.red(`PostController.toggleLike() => ${err}`));
    } finally {
        if (conn) conn.release();
    }
};

const PostController = {
    getAllPosts,
    postPost,
    getPostImage,
    deletePost,
    toggleLike,
};

export default PostController;
