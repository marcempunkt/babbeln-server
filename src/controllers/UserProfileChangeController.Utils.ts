import { Express, Request } from "express";
import multer, { Multer, FileFilterCallback } from "multer";
import Joi, { ValidationResult } from "@hapi/joi";
import colors from "colors";
import sharp from "sharp";
import fs from "fs";
import path from "path";

const fileFilter = (req: Request, file: Express.Multer.File, cb: FileFilterCallback) => {
  enum AcceptableFileType {
    PNG = "image/png",
    JPG = "image/jpeg",
    GIF = "image/gif",
  };

  if (file.mimetype === AcceptableFileType.PNG ||
      file.mimetype === AcceptableFileType.JPG ||
      file.mimetype === AcceptableFileType.GIF) {
    file.filename= "1";
    return cb(null, true);
  } else {
    console.error(colors.red("ProfileChangeControllerUtils.fileFilter() => wrong file type"));
    return cb(null, false);
  }
};

/* Multer does not work with typescript
 * TODO expand the type declaration of upload & Multer to make the code below work */
// @ts-ignore
const upload: Multer = multer({
  // storage: storage,
  dest: "files/user/profile",
  limits: { files: 1, fileSize: 1068576 },
  fileFilter: fileFilter
}).single("avatar");

const renameAvatar = async (file: Express.Multer.File, userId: number) => {
  await sharp(file.path)
    .resize(200, 200)
    .flatten({ background: "#000" })
    .gif()
    .toFile(path.join(file.destination, `${userId}.gif`))
    .catch((err: unknown) => console.error(colors.red(`ProfileChangeControllerUtils.renameAvatar() => ${err}`)));
}

const deleteAvatar = async (file: Express.Multer.File) => {
  await fs.promises.rm(file.path)
	  .catch((err: unknown) => console.error(colors.red(`ProfileChangeControllerUtils.deleteAvatar() => ${err}`)));
  // console.log(`/ProfileChangeControllerUtils.deleteAvatar() => removed ${file.path}`);
};

const changePasswordValidation: (requestBody: { password: string }) => ValidationResult<{ password: string }> = (requestBody) => {
  /**
   * Validation for when the password is being changed
   */
  let schema = {
    password: Joi.string().min(8).max(1024).required()
  }
  return Joi.validate(requestBody, schema);
};

const UserProfileChangeControllerUtils = {
  upload,
  renameAvatar,
  deleteAvatar,
  changePasswordValidation,
};

export default UserProfileChangeControllerUtils;
