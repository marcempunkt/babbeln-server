import { Request, Response } from "express";
import fs from "node:fs";
import path from "node:path";
import colors from "colors";
import { PoolConnection } from "mariadb";

import pool from "../db/db";
import type { Status } from "../ts/types/db_types";
import type { ResponseBody } from "../ts/types/express_types";

const getAvatar = async (
    req: Request<{ userId: string }>,
    res: Response<Buffer | ResponseBody>
) => {
    /**
     * Get the profile picture of a user
     * 
     * @param    {number}  userId
     * 
     * @returns  {gif}
     */
    const userId: number = Number(req.params.userId);
    /* Profile picture */ 
    const avatarPath: string = path.join(__dirname, "../../", "files", "user", "profile", userId + ".gif");
    const defaultAvatarPath: string = path.join(__dirname, "../../", "files", "user", "profile", "default" + ".gif");

    const avatar: Buffer | null = await fs.promises.readFile(avatarPath).catch((_) => null);
    if (avatar) return res.type("gif").send(avatar);
    const dAvatar: Buffer | null = await fs.promises.readFile(defaultAvatarPath).catch((_) => null);
    if (dAvatar) return res.type("gif").send(dAvatar);
    return res.status(500).send({ message: "Could not get avatar." });
};

const getStatus = async (
    req: Request<{ userId: string }>,
    res: Response<Status | ResponseBody>
) => {
    /**
     * Get the status of a user
     * 
     * @param  {userId}  number
     * 
     * @returns  {Status}
     */
    const userId: number = Number(req.params.userId);

    let conn: PoolConnection | undefined;

    try {
        conn = await pool.getConnection();
        if (!conn) throw console.error(colors.red(`UserProfieleController.getStatus() => conn is undefined!`));

        const query: string = `SELECT status FROM users WHERE id=?`;
        const queryResult = await conn.query(query, [userId]);
        const resultObject: { status: Status } | undefined = queryResult[0];

        if (!resultObject) return res.status(400).send({ message: `Couldn't get status of user with the id ${userId}` });

        const status: Status = resultObject.status;

        return res.send(status);

    } catch (err: unknown) {
        res.status(500).send({ message: `Couldn't get status of user with the id ${userId}` })
        throw console.log(colors.red(`UserProfileController.getStatus() => ${err}`));
    } finally {
        if (conn) conn.release();
    }
};

const getRegisterDate = async (
    req: Request<{ userId: string }>,
    res: Response<number | ResponseBody>
) => {
    /**
     * Get the register date of a user
     * 
     * @param    {number}  userId
     * 
     * @returns  {number}  register date
     */
    const userId: number = Number(req.params.userId);

    let conn: PoolConnection | undefined;

    try {
        conn = await pool.getConnection();
        if (!conn) throw console.error(colors.red(`UserProfileController.getRegisterDate() => conn is undefined!`));

        const query: string = `SELECT register_date FROM users WHERE id=?`;
        const queryResult = await conn.query(query, [userId]);
        const resultObject: { register_date: number } | undefined = queryResult[0];

        if (!resultObject) return res.status(400).send({ message: `Couldn't get the register date of user with the id ${userId}` });

        const registerDate: number = queryResult[0].register_date;

        return res.send(registerDate);

    } catch (err: unknown) {
        res.status(500).send({ message: `Couldn't get the register date of user with the id ${userId}` });
        throw console.log(colors.red(`UserProfileController.getRegisterDate() => ${err}`));
    } finally {
        if (conn) conn.release();
    }
};

const UserProfileController = {
    getAvatar,
    getStatus,
    getRegisterDate,
};

export default UserProfileController;
