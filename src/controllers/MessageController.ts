import colors from "colors";
import pool from "../db/db";
import type { Response, Request } from "express";
import type { SqlError, PoolConnection } from "mariadb";
import type { OkPacket } from "../ts/types/mariadb_types";
import type { Message as ServerMessage } from "../ts/types/db_types";
import type { SendMessage } from "../ts/types/client_types";
import type { ResponseBody, AuthLocals } from "../ts/types/express_types";

const getMessages = async (
    req: Request,
    res: Response<Array<ServerMessage> | ResponseBody, AuthLocals>
) => {
    /**
     * Get all messages of an user
     */
    let conn: PoolConnection | undefined;

    try {
        conn = await pool.getConnection();
        if (!conn) throw console.error(
            colors.red(`MessageController.getMessages() => conn is undefined!`)
        );

        const queryGetMsgs: string = `SELECT * FROM messages WHERE owner=?`;
        const queryGetMsgsResult = await conn.query(queryGetMsgs, [res.locals.userId]);
        const messages: Array<ServerMessage> = [...queryGetMsgsResult];

        return res.send(messages);

    } catch (err: unknown) {
        res.status(500).send({ message: `Failed to get chat messages.` });
        throw console.error(colors.red(`MessageController.getMessages() => ${err}`));
    } finally {
        if (conn) conn.release();
    }
};

const sendMessage = async (
    req: Request<{}, {}, { msg: SendMessage }, {}, {}>,
    res: Response<Array<ServerMessage | undefined> | ResponseBody, AuthLocals>
) => {
    /**
     * Save message to DB and return both messages
     * (one for the sender and one for the receiver)
     * back to the sender of this message
     */
    if (!req.body.msg.content) return res.status(400).send({ message: "Message is empty." });

    let conn: PoolConnection | undefined;

    try {
        conn = await pool.getConnection();
        if (!conn) throw console.error(colors.red("MessageController.sendMessage => conn is undefined!"));

        /** CHECK IF BLOCKED */
        const getIsBlocked: string = `SELECT blocked FROM friends WHERE user_id=? AND friend_with=?`;
        const getIsBlockedResult = await conn.query(getIsBlocked, [req.body.msg.to_user, req.body.msg.from_user]);
        const isBlocked: boolean = (getIsBlockedResult[0].blocked) ? true : false;

        const msgSendAt: number = new Date().valueOf(); // timestamp for msg

        /** Private function to save a message */
        type SaveMessageFn = (owner: number,
                              fromUser: number,
                              toUser: number,
                              content: string,
                              sendAt: number) => Promise<ServerMessage | undefined>;
        const saveMessage: SaveMessageFn  = async (owner, fromUser, toUser, content, sendAt) => {
            if (!conn) throw console.error(colors.red("MessageController.sendMessage().saveMessage() => conn is undefined!"));

            const querySaveMsg: string = `INSERT INTO
                                           messages(owner, from_user, to_user, content, send_at)
                                         VALUES (?, ?, ?, ?, ?)`;

            const contentTrimmed: string = content.trim();

            const querySaveMsgResult: OkPacket = await conn.query(querySaveMsg, [owner, fromUser, toUser, contentTrimmed, sendAt]);
            console.log(`MessageController.sendMessage().saveMessage() => saved message of owner(${owner}) to db.`);

            const queryGetSavedMsg: string = `SELECT 
                                                * 
                                              FROM 
                                                messages
                                              WHERE 
                                                owner=?
                                              AND 
                                                content=?
                                              AND 
                                                send_at=?`;

            const queryGetSavedMsgResult = await conn.query(queryGetSavedMsg, [owner, contentTrimmed, sendAt]);
            const savedMsg: ServerMessage | undefined = queryGetSavedMsgResult[0];
            return savedMsg;
        };

        let savedMessageForSender: ServerMessage | undefined;
        let savedMessageForReceiver: ServerMessage | undefined;

        if (isBlocked) {
            /** SAVE MSG ONLY FOR FROM_USER */
            savedMessageForSender = await saveMessage(req.body.msg.from_user,
                                                      req.body.msg.from_user,
                                                      req.body.msg.to_user,
                                                      req.body.msg.content,
                                                      msgSendAt);
            /** CASE SERVER ERROR */
            if (!savedMessageForSender) {
	        console.error(colors.red(`MessageController/sendMessage() => couldn't save the message in the db`));
	        return res.status(500).send({ message: `Couldn't save message to the db` });	
            }

        }

        if (!isBlocked) {
            /** EVERYTHING OKAY */
            savedMessageForSender = await saveMessage(req.body.msg.from_user,
                                                      req.body.msg.from_user,
                                                      req.body.msg.to_user,
                                                      req.body.msg.content,
                                                      msgSendAt);
            savedMessageForReceiver = await saveMessage(req.body.msg.to_user,
                                                        req.body.msg.from_user,
                                                        req.body.msg.to_user,
                                                        req.body.msg.content,
                                                        msgSendAt);

            /** CASE SERVER ERROR */
            if (!savedMessageForSender || !savedMessageForReceiver) {
	        console.error(colors.red(`MessageController/sendMessage() => couldn't save the messages in the db`));
	        return res.status(500).send({ message: `Couldn't save message to the db` });	
            }
        }

        /** OKAY */
        return res.send([savedMessageForSender, savedMessageForReceiver]);

    } catch (err: unknown) {
        res.status(500).send({ message: "Failed to send message." });
        throw console.error(colors.red(`MessageController.sendMessage() => ${err}`));
    } finally {
        if (conn) conn.release();
    }
};

const deleteGloballyMessage = async (
    req: Request<{ msgId: string }>,
    res: Response<ResponseBody & { deletedMessages?: Array<ServerMessage> }, AuthLocals>
) => {
    /**
     * Delete a single message for all users
     */

    /** CHECK MSG ID */
    const msgId: number = Number(req.params.msgId);
    if (!msgId) return res.status(400).send({ message: `Bad message id.` });

    let conn: PoolConnection | undefined;

    try {
        conn = await pool.getConnection();
        if (!conn) throw console.error(colors.red(`MessageController.deleteGloballyMessage() => conn is undefined!`));

        /** CHECK IF USER IS AUTHORIZED */
        const queryGetAuthorId: string = `SELECT from_user FROM messages WHERE owner=? AND from_user=? AND id=?`;
        const queryGetAuthorIdResult: Array<{ from_user: number }> = await conn.query(
            queryGetAuthorId,
            [res.locals.userId, res.locals.userId, msgId]
        );
        const getAuthorIdResult: { from_user: number } | undefined = queryGetAuthorIdResult[0];

        if (!getAuthorIdResult) return res.status(401).send({ message: `You aren't authorized to delete this message.` });
        const authorId: number = getAuthorIdResult.from_user;
        if (authorId !== res.locals.userId) return res.status(401).send({ message: `You aren't authorized to delete this message.` });

        /** GET TO DELETE MSG */
        const queryGetMsg: string = `SELECT * FROM messages WHERE id=?`;
        const queryGetMsgResult = await conn.query(queryGetMsg, [msgId]);
        const message: ServerMessage | undefined = queryGetMsgResult[0];

        if (!message) {
            console.log(colors.red(`MessageController.getGloballyMessage() => no message was found with the id ${msgId}`));
            return res.status(500).send({ message: `There is no message that can be deleted.`});
        }

        /** GET MSG OF BOTH (OR IN GROUPCHAT MORE) USERS */
        const queryGetAllMsgs = `SELECT * FROM messages WHERE send_at=? AND content=?`;
        const queryGetAllMsgsResult = await conn.query(queryGetAllMsgs, [message.send_at, message.content]);
        const allMessages: Array<ServerMessage> = [...queryGetAllMsgsResult];
        const allMessageIds: Array<number> = allMessages.map((msg: ServerMessage) => msg.id);

        if (!allMessageIds.length) {
            console.error(colors.red(
	        `MessageController.deleteGloballyMessage => Didn't found more message to delete ((!This should never happen!)) id: ${msgId}`
            ));
            return res.status(500).send({ message: "Couldn't find any messages to delete "});
        }

        /** DELETE ALL MSGS
         * create the whereClause according to how many matches we got (also works for group chats!) *
         * Example: 
         * DELETE FROM messages
         * WHERE id=${1. MessageId}
         * WHERE id=${2. MessageId} OR
         * WHERE id=${3. MessageId} OR
         * WHERE id=${4. MessageId}
         */
        let whereClause: string = "WHERE";
        allMessageIds.map((msgId: number, index: number) => {
            if (index === (allMessageIds.length - 1)) {
	        whereClause += ` id=${msgId}`;
            } else {
	        whereClause += ` id=${msgId} OR`;
            }
        });

        const queryDeleteAllMsgs: string = `DELETE FROM messages ${whereClause}`;
        const queryDeleteAllMsgsResult: OkPacket = await conn.query(queryDeleteAllMsgs);

        /** OKAY */
        return res.send({
            message: `Successfully deleted message for everyone!`,
            deletedMessages: allMessages,
        });

    } catch (err: unknown) {
        res.status(500).send({ message: "Failed to delete messages." });
        throw console.error(colors.red(`MessageController.deleteGloballyMessage() => ${err}`));
    } finally {
        if (conn) conn.release();
    }
};

const deletePrivatelyMessage = async (
    req: Request<{ msgId: string }>,
    res: Response<ResponseBody, AuthLocals>
) => {
    /**
     * Delete a single message for only one user
     */

    /** CHECK MSG ID */
    const msgId: number = Number(req.params.msgId);
    if (!msgId) return res.status(400).send({ message: "Bad message id" });

    let conn: PoolConnection | undefined;

    try {
        conn = await pool.getConnection();
        if (!conn) throw console.error(colors.red(`MessageController.deletePrivatelyMessage() => conn is undefined!`));

        /* DELETE MSG */
        const query: string = `DELETE FROM messages WHERE id=?`;

        conn.query(query, [msgId]).then((row: OkPacket) => {
            /** OKAY */
            console.log(`MessageController.deletePrivatelyMessage() => Message deleted: ${JSON.stringify(row)}`);
            return res.send({ message: "Message was deleted only for you" });
        }).catch((err: SqlError) => {
            /** CASE SERVER ERROR */
            console.log(colors.red(`MessageController.deletePrivatelyMessage() => Couldn't delete message with the id ${msgId} => ${err}`));
            return res.status(500).send({ message: `Server Eror: Couldn't delete message with the id ${msgId}`});
        });

    } catch (err: unknown) {
        res.status(500).send({ message: "Failed to delete message." });
        throw console.error(colors.red(`MessageController.deletePrivatelyMessage() => ${err}`));
    } finally {
        if (conn) conn.release();
    }
};

const deleteChat = async (
    req: Request<{}, {}, { fromUserId: number; toDeleteUserId: number }>,
    res: Response<ResponseBody, AuthLocals>
) => {
    /**
     * Delete a whole chat (but privately (only for one user))
     */

    /** CHECK USER IDS */
    const fromUserId: number = Number(req.body.fromUserId);
    const toDeleteUserId: number = Number(req.body.toDeleteUserId);

    if (!fromUserId || !toDeleteUserId) return res.status(400).send({ message: "Missing fromUserId and/or toDeleteUserId" });

    let conn: PoolConnection | undefined;

    try {
        conn = await pool.getConnection();
        if (!conn) throw console.error(colors.red(`MessageController.deleteChat() => conn is undefined!`));

        /** DELETE CHAT */
        const query: string = `DELETE FROM messages WHERE owner=? AND (from_user=? OR to_user=?);`;

        conn.query(query, [fromUserId, toDeleteUserId, toDeleteUserId]).then((row: OkPacket) => {
            /** OKAY */
            console.log(`MessageController.deleteChat() => Chat deleted: ${JSON.stringify(row)}`);
            return res.send({ message: "Chat has been deleted." });
        }).catch((err: SqlError) => {
            /** CASE SERVER ERROR */
            console.log(colors.red(`MessageController.deleteChat() => Couldn't delete chat => toDeleteUserId: ${toDeleteUserId} => ${err}`));
            return res.status(500).send({ message: `Couldn't delete chat of user: ${toDeleteUserId}`});
        });

    } catch (err: unknown) {
        res.status(500).send({ message: "Failed to delete chat." });
        throw console.error(colors.red(`MessageController.deleteChat() => ${err}`));
    } finally {
        if (conn) conn.release();
    }
};

const MessageController = {
    getMessages,
    sendMessage,
    deleteGloballyMessage,
    deletePrivatelyMessage,
    deleteChat,
};

export default MessageController;
