import process from "node:process";
import colors from "colors";

const checkEnv: () => void = () => {
  if (!process.env.DB_HOST ||
      !process.env.DB_USER ||
      !process.env.DB_PASS ||
      !process.env.DB_NAME ||
      !process.env.SECRET_TOKEN   ) {
    /* .env is missing or uncomplete */
    console.error(colors.red(`Missing fields in dotenv: `));
    console.error({ DB_HOST: process.env.DB_HOST });
    console.error({ DB_USER: process.env.DB_USER });
    console.error({ DB_PASS: process.env.DB_PASS });
    console.error({ DB_NAME: process.env.DB_NAME });
    console.error({ SECRET_TOKEN: process.env.SECRET_TOKEN });
    process.exit(1);
  }
};

export default checkEnv;
