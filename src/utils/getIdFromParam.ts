const getIdFromParam: (param: string) => number | null = (param) => {
  const id: number = Number(param);
  if (Number.isInteger(id) && id > 0) return id;
  return null; 
};

export default getIdFromParam;
