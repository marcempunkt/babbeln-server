import { TinyInt } from "./mariadb_types";

export enum Status {
  ON = "on",
  OFF = "off",
  DND = "dnd",
  AWAY = "away",
};

export interface User {
  id: number;
  username: string;
  email: string;
  password: string;
  is_active: TinyInt;
  socketid: string;
  status: Status;
  register_date: number;
};

export interface Message {
  id: number;
  owner: number;
  from_user: number;
  to_user: number;
  content: string;
  send_at: number;
};

/* the table in sql is friendrequests but inside the code its often referred as pending */
export interface Friendrequest {
  id: number;
  from_user: number;
  to_user: number;
  accepted: TinyInt;
};

export interface Friend {
  id: number;
  user_id: number;
  friend_with: number;
  blocked: TinyInt;
};

export interface Post {
  id: number;
  written_by: number;
  content: string;
  written_at: number;
};

export interface PostComment {
  id: number;
  post_id: number;
  user_id: number;
  content: string;
  written_at: number;
};

export interface Socketid {
  socketid: string;
  user_id: number;
};
