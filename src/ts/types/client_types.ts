import { TinyInt } from "./mariadb_types";
import { Message as ServerMessage } from "./db_types";

/* Pending is the Client's version of Friendrequest */
export interface Pending {
  pendingId: number;
  fromUser: number;
  toUser: number
};

export interface Friend {
  friendId: number;
  friendName: string;
  status: string;
  registerDate: number;
  blocked: TinyInt;
};

export enum MessageType {
  SEND_MSG = "send-msg",
  REC_MSG = "rec-msg",
};

export interface Message extends ServerMessage {
  is_deleted: TinyInt;
};

export interface SendMessage {
  from_user: number;
  to_user: number;
  content: string;
}

export interface Post {
  id: number;
  writtenBy: number;
  content: string;
  writtenAt: number;
  images: Array<string>;
  likes: Array<number>;
  comments: Array<PostComment>;
};

export interface PostComment {
  commentId: number;
  userId: number;
  content: string;
  writtenAt: number;
};
