import supertest from "supertest";
import app from "../main";

describe("message", () => {
    describe("get message route", () => {
        describe("given the product does not exist", () => {
            it("should return a 404", async () => {
                await supertest(app).get(`/message/`).expect(401);
            })
        })
    })
});
