worker_processes 1;
events {
    worker_connections 1024;
}

http {
    ### -------------------------------------------------------------- babbeln.app
    server {
        server_name babbeln.app www.babbeln.app;
        listen 80;
        listen [::]:80;
        listen 443 ssl;
        listen [::]:443 ssl;
        
        ssl_certificate /etc/letsencrypt/live/babbeln.app/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/babbeln.app/privkey.pem;

        include /etc/letsencrypt/options-ssl-nginx.conf;
        ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;

        location /.well-known/acme-challenge/ {
            # Directory of the letsencrypt certificates
            allow all;
            root /var/www/;
        }

        location / {
            proxy_pass http://babbelnweb-babbelnclient:80;
        }
    } 
    ### -------------------------------------------------------------- server.babbeln.app
    server {
        server_name server.babbeln.app www.server.babbeln.app;
        listen 80;
        listen [::]:80;
        listen 443 ssl;
        listen [::]:443 ssl;

        ssl_certificate /etc/letsencrypt/live/server.babbeln.app/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/server.babbeln.app/privkey.pem;

        include /etc/letsencrypt/options-ssl-nginx.conf;
        ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;

        location /.well-known/acme-challenge/ {
            # Directory of the letsencrypt certificates
            allow all;
            root /var/www/;
        }

        location / {
            if ($request_method ~* "(GET|POST|DELETE|PATCH)") {
                add_header 'Access-Control-Allow-Origin' '*' always;
                add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS, DELETE, PATCH' always;
                add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range' always;
                add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range' always;
            }

            if ($request_method = 'OPTIONS') {
                add_header 'Access-Control-Allow-Origin' '*';
                # add_header 'Access-Control-Allow-Credentials' 'true';
                add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS, DELETE, PATCH';
                # Custom headers and headers various browsers *should* be OK with but aren't
                add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range';
                # add_header 'Access-Control-Allow-Headers' 'Authorization,Accept,Origin,DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range';
                add_header 'Access-Control-Max-Age' 1728000; # Tell client that this pre-flight info is valid for 20 days
                add_header 'Content-Type' 'text/plain; charset=utf-8';
                add_header 'Content-Length' 0;
                return 204;
            }

            proxy_set_header host $host;
            proxy_set_header X-real-ip $remote_addr;
            proxy_set_header X-forwarded-for $proxy_add_x_forwarded_for;
            proxy_pass http://host.docker.internal:3001;
        }

        location /socket.io/ {
            proxy_set_header host $host;
            proxy_set_header X-real-ip $remote_addr;
            proxy_set_header X-forwarded-for $proxy_add_x_forwarded_for;

            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "upgrade";
            proxy_read_timeout 43200000;

            proxy_pass http://host.docker.internal:3001;
        }
    } 
    ### -------------------------------------------------------------- turn.babbeln.app 
    server {
        server_name turn.babbeln.app www.turn.babbeln.app;
        listen 80;
        listen [::]:80;
        listen 443 ssl;
        listen [::]:443 ssl;
        
        ssl_certificate /etc/letsencrypt/live/turn.babbeln.app/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/turn.babbeln.app/privkey.pem;

        include /etc/letsencrypt/options-ssl-nginx.conf;
        ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;

        location /.well-known/acme-challenge/ {
            # Directory of the letsencrypt certificates
            allow all;
            root /var/www/;
        }
    } 
    ### -------------------------------------------------------------- bots
    server {
        listen 80;
        listen [::]:80;
        server_name "";
        return 400;
    }
}

### -------------------------------------------------------------- UPD/Coturn 
stream {
    server {
        listen 5349 ssl;
        listen [::]:5349 ssl;

        ssl_certificate /etc/letsencrypt/live/turn.babbeln.app/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/turn.babbeln.app/privkey.pem;
        # server_name turn.babbeln.app www.turn.babbeln.app;

        ssl_preread on;
        proxy_buffer_size 10m;
        proxy_pass host.docker.internal:3478;
    }
}

